<?php
/**
 * Class cAPI
 * Providers properties of every API-class.
 *
 * Author   Alexander Gaal <alexander.gaal@live.at>
 * Release  2016-08-26
 * Version  0.1
 */
class cAPI implements iAPI {
    protected $data      = [];
    protected $method    = null;
    protected $tablename = null;

    public function getAll() {
        $records = [];

        return json_encode($records);
    }

    public function getByID($data) {
        $record  = [$data];

        return json_encode($record);
    }

    public function post($data) {

    }

    public function put($data) {

    }

    public function delete($data) {

    }
}