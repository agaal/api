<?php
/**
 * Class APIHandler
 * Handles every request on the API-system and call the specific API-class.
 *
 * Author   Alexander Gaal <alexander.gaal@live.at>
 * Release  2016-08-26
 * Version  0.1
 */
class APIHandler {
    private $api     = null;
    private $request = null;

    public function __construct(Request $request) {
        $this->setRequest($request);
    }

    private function setRequest($r) {
        if(Comparator::isNull($r)) {
            throw new nullpointerexception("Cannot handle an empty request.");
        }

        $this->request = $r;
    }

    private function getRequest() {
        return $this->request;
    }
}