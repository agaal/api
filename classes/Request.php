<?php
/**
 * Class Request
 *
 * Author   Alexander Gaal <alexander.gaal@live.at>
 * Release  2016-08-26
 * Version  0.1
 */
class Request {
    private $request = [];

    public function __construct($request) {
        $this->setRequest($request);
    }

    private function setRequest($request) {
        if(empty($request)) {
            throw new invalidargumentexception("Cannot handle an empty request.");
        }

        $this->request = $request;
    }

    private function getRequest() {
        return $this->data;
    }

    /**
     * Add or update a key of request with value.
     * @param $key
     * @param $value
     */
    public function set($key, $value) {
        $this->getRequest()[$key] = $value;
    }

    /**
     * Returns a specific key.
     * @param $key
     * @return mixed
     */
    public function get($key) {
        return $this->getRequest()[$key];
    }
}