<?php
/**
 * Interface iAPI
 * This interface provide the default functions for every API request.
 *
 * Author   Alexander Gaal <alexander.gaal@live.at>
 * Release  2016-08-26
 * Version  0.1
 */
interface iAPI {
    /**
     * Returns a json-formatted string of all records.
     * @return json
     */
    public function getAll();

    /**
     * Returns a specific record.
     * @param  $data
     * @return json
     */
    public function getByID($data);

    /**
     * Insert a new record.
     * @param  $data
     * @return void
     */
    public function post($data);

    /**
     * Updates a specific record.
     * @param  $data
     * @return void
     */
    public function put($data);

    /**
     * Removes a specific record.
     * @param  $data
     * @return void
     */
    public function delete($data);
}