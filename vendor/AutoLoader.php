<?php
/**
 * Die AutoLoader-Klasse ÃƒÂ¼bernimmt die Aufgabe, Klassen, Interfaces einzubinden, welche vom Skript nicht erkannt werden.
 * Es wird in den hinterlegten Ordnern nachgeschaut, ob eine derartige Datei existiert und eingebunden.
 *
 * @author        Alexander Gaal
 * @version       1.2
 *
 * @var   $roots     Die Ordner, die nach Klassen durchsucht werden.
 * @var   $dirs      Unterordner der Root-Verzeichnisse.
 * @const extension  Die Dateiendung um die Dateien einzubinden.
 */
class AutoLoader {
    private static $roots     = array('/');
    private static $dirs      = [];

    const EXTENSION    = '.php';

    /**
     * Registriert eine Instanz des AutoLoaders oder liefert das bereits instanziierte Objekt zurÃƒÂ¼ck.
     */
    public static function register(array $dirs = []) {
        if(count($dirs) <= 0) {
            foreach(self::$roots as $root) {
                self::$dirs = array_merge(self::$dirs, self::subdirs($root));
            }
        } else {
            self::$dirs = $dirs;
        }

        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    /**
     * Die autoload-Funktion greift einen Parameter ab und probiert die vordefinierten Verzeichnisse durch, ob die Datei existiert.
     *
     * @param  $include  Erwartet einen Dateinamen um auf Existenz zu prÃƒÂ¼fen.
     */
    public static function autoload($include) {
        if(empty($include)) {
            throw new Exception('Cannot autoload class, parameter is not set.');
        }

        $dirs      = self::$dirs;
        $extension = self::EXTENSION;

        foreach($dirs as $dir) {
            $file   =   $dir . '/' . $include . $extension;

            if(is_readable($file)) {
                require_once($file);
            }
        }
    }

    /**
     * Diese Funktion prÃƒÂ¼ft das Verzeichnis auf Unterverzeichnisse und fÃƒÂ¼gt diese dem static-Array hinzu.
     *
     * @param  $root  Erwartet ein Verzeichnis, welches geprÃƒÂ¼ft werden soll.
     */
    private static function subdirs($root) {
        $dirs = [];

        if(!is_readable($root)) {
            throw new Exception('Cannot read ' . $root);
        }

        array_push($dirs, $root);

        foreach(new DirectoryIterator($root) as $dir) {
            if($dir->isDir() && !($dir->isDot())) {
                $dirName = $root . '/' . $dir->getFilename();

                $dirs = array_merge($dirs, self::subdirs($dirName));
            }
        }

        return $dirs;
    }
}
