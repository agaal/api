<?php
/**
 * Class Comparator
 * Compares different parameter with eachother. Depending on $strict this class compares type-sensitive.
 */
class Comparator {
    /**
     * Check if $a is equal $b.
     * @param  $a
     * @param  $b
     * @param  bool $strict
     * @return bool
     */
    public static function isEqual($a, $b, $strict = true) {
        if($strict) {
            return $a === $b;
        }

        return $a == $b;
    }

    /**
     * Check if $a is null.
     * @param  $a
     * @param  bool $strict
     * @return bool
     */
    public static function isNull($a, $strict = true) {
        if($strict) {
            return $a === null;
        }

        return $a == null;
    }
}