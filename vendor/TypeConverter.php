<?php
/**
 * Converts values and arrays into a specific type.
 * Class TypeConverter
 */
class TypeConverter {
    /**
     * Converts a value into a int. If a object is given, it throws an invalidargumentexception. If an array is given, it converts all elements of this array.
     * @param  $param
     * @return array|float
     */
    public static function int($param) {
        if(is_object($param)) {
            throw new InvalidArgumentException("Cannot convert an object to int.");
        }

        if(is_array($param)) {
            return self::intArray($param);
        }

        return (int) $param;
    }

    /**
     * Converts every value of an array into a float.
     * @param  $param
     * @return array
     */
    private static function intArray($param) {
        $data = [];

        if(!is_array($param)) {
            throw new InvalidArgumentException("Cannot handle a single value as array.");
        }

        foreach($param as $p) {
            $data[] = self::int($p);
        }

        return $data;
    }

    /**
     * Converts a value into a float. If a object is given, it throws an invalidargumentexception. If an array is given, it converts all elements of this array.
     * @param  $param
     * @return array|float
     */
    public static function float($param) {
        if(is_object($param)) {
            throw new InvalidArgumentException("Cannot convert an object to float.");
        }

        if(is_array($param)) {
            return self::floatArray($param);
        }

        return (float) $param;
    }

    /**
     * Converts every value of an array into a float.
     * @param  $param
     * @return array
     */
    private static function floatArray($param) {
        $data = [];

        if(!is_array($param)) {
            throw new InvalidArgumentException("Cannot handle a single value as array.");
        }

        foreach($param as $p) {
            $data[] = self::float($p);
        }

        return $data;
    }

    /**
     * Converts a value into a double. If a object is given, it throws an invalidargumentexception. If an array is given, it converts all elements of this array.
     * @param  $param
     * @return array|float
     */
    public static function double($param) {
        if(is_object($param)) {
            throw new InvalidArgumentException("Cannot convert an object to double.");
        }

        if(is_array($param)) {
            return self::doubleArray($param);
        }

        return (double) $param;
    }

    /**
     * Converts every value of an array into a double.
     * @param  $param
     * @return array
     */
    private static function doubleArray($param) {
        $data = [];

        if(!is_array($param)) {
            throw new InvalidArgumentException("Cannot handle a single value as array.");
        }

        foreach($param as $p) {
            $data[] = self::double($p);
        }

        return $data;
    }

    /**
     * Converts a value into a boolean. If a object is given, it throws an invalidargumentexception. If an array is given, it converts all elements of this array.
     * @param  $param
     * @return array|float
     */
    public static function boolean($param) {
        if(is_object($param)) {
            throw new InvalidArgumentException("Cannot convert an object to boolean.");
        }

        if(is_array($param)) {
            return self::booleanArray($param);
        }

        return (boolean) $param;
    }

    /**
     * Converts every value of an array into a boolean.
     * @param  $param
     * @return array
     */
    private static function booleanArray($param) {
        $data = [];

        if(!is_array($param)) {
            throw new InvalidArgumentException("Cannot handle a single value as array.");
        }

        foreach($param as $p) {
            $data[] = self::boolean($p);
        }

        return $data;
    }
}